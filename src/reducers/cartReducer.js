const initState = {
    cartItems: [
        {
            id:0,
            count: 1,
            unitPrice: 75,
            description: {
                header: `Title Lorem ipsum dolor sit`,
                details: `Lorem ipsum dolor sit amet, quis dictum maurius
                          erat aliquam, ac in pede pharetra quis non et.`
            }
        },
        {
            id:1,
            count: 3,
            unitPrice: 25,
            description: {
                header: `Title Lorem ipsum dolor sit`,
                details: `Lorem ipsum dolor sit amet, quis dictum maurius
                          erat aliquam, ac in pede pharetra quis non et.`
            }
        },
        {
            id:2,
            count: 5,
            unitPrice: 35,
            description: {
                header: `Title Lorem ipsum dolor sit`,
                details: `Lorem ipsum dolor sit amet, quis dictum maurius
                          erat aliquam, ac in pede pharetra quis non et.`
            }
        }
    ]
}

const cartReducer = (state=initState, action) => {
    switch(action.type){

        case 'INCR_ITEM':
            return Object.assign({}, state, {
                cartItems: state.cartItems.map((cartItem) => {
                    if (cartItem.id === action.id){
                        return Object.assign({}, cartItem, {
                            count: cartItem.count+1
                        })
                    }
                    return cartItem;
                })
            })

        case 'DECR_ITEM':

            return Object.assign({}, state, {
                cartItems: state.cartItems.map((cartItem) => {
                    if(cartItem.id === action.id){
                        if(cartItem.count === 0) return cartItem;
                        return Object.assign({}, cartItem, {
                            count: cartItem.count-1
                        })
                    }

                    return cartItem;

                })
            })

        case 'DELETE_ITEM':
            return { cartItems:  state.cartItems.filter( ({ id }) => id !== action.id )};
        
        default: 
            return state
    }
}

export default cartReducer;
