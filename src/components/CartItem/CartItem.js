import React from 'react';
import './CartItem.css';
import MdDelete from 'react-icons/lib/md/delete';
import IoImage from 'react-icons/lib/md/image';

const CartItem = ({incrCount, decrCount, deleteItem, count, description, unitPrice}) => (
    <div className="CartItem">
        <div className="CartItem__image">
            <div className="CartItem__image__icon">
             <IoImage size={"inherit"}/>
            </div>
            
        </div>
        <div className="CartItem__text">
            <div className="CartItem__text--header">
                {description.header}
            </div>
            <div className="CartItem__text--main">
                {description.details}
                
            </div>
        </div>
        <div className="CartItem__controls">
            <div className="CartItem__controls--upper">
                <div className="CartItem__delete">
                    <MdDelete onClick={deleteItem} size={"inherit"}/>
                </div>
            </div>
            <div className="CartItem__controls--lower">
                <button onClick={decrCount} >-</button>
                <div className="CartItem__count">{ count }</div>
                <button onClick={incrCount} >+</button>
                <span className="CartItem__priceTag">{unitPrice*count} €</span>
            </div>
        </div>
    </div>
)

export default CartItem;