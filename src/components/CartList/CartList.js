import React from 'react';
import { Link } from "react-router-dom";
import CartItem from '../CartItem/CartItem.js';
import classnames from 'classnames';

import './CartList.css';

const CartList = ({ cartItems, incrCount, decrCount, deleteItem }) => {
    let productCount = cartItems.reduce((accumulator, item) => accumulator + (item.unitPrice*item.count), 0);

    return(
        <div className="CartList">
            {cartItems.map(cartItem => (
                    <CartItem 
                        key={cartItem.id}
                        {...cartItem}
                        incrCount={ () => incrCount(cartItem.id) }
                        decrCount={ () => decrCount(cartItem.id) }
                        deleteItem={ () => deleteItem(cartItem.id) }
                    />
                )
            )}
            <div className="CartList__priceTag">
              <div>
                {productCount} €
              </div>
              <Link to="shipping">
                <button disabled={productCount===0}>Buy</button>
             </Link>
            </div>
        </div>
    )
}

export default CartList;