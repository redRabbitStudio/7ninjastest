import React, {Component} from 'react';
import validate from 'validator';
import classnames from 'classnames';
import './Form.css';

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields:{
                name: '',
                address:'',
                phone:'',
                email: '',
                shippingOptions:''
            },
            fieldErrors: {
            }
        };
    }

    validate = (fields) => {
        const errors = {};
        if(!fields.name) errors.name = 'Name required';
        if(!fields.email) errors.email = 'Email required';
        if(!fields.address) errors.address = 'Address required';
        if(fields.email && !validate.isEmail(fields.email)) errors.email = 'Email incorrect';
        return errors;
    }

    onInputChange = (evt) => {
        const fields = {...this.state.fields};
        console.log('all',Object.keys(this.state.fieldErrors))
        const resetField = Object.keys(this.state.fieldErrors).filter(key => key===evt.target.name);
        
        const fieldErrors = {...this.state.fieldErrors};
        if(resetField) {
            fieldErrors[resetField] = '';
        };

        fields[evt.target.name] = evt.target.value;
        this.setState({ 
            fields,
            fieldErrors
         });
    }

    onFormSubmit = (evt) => {
        evt.preventDefault();
        const fields = {...this.state.fields};        
        const fieldErrors = this.validate(fields);
        this.setState({ fieldErrors });
        if(Object.keys(fieldErrors).length) return;
    }

    render() {
        const errorClass ={
            name: classnames({
                    "Form__invalid": true,
                    "Form__invalid--show": this.state.fieldErrors.name
                    }),
            address: classnames({
                    "Form__invalid": true,
                    "Form__invalid--show": this.state.fieldErrors.address
                    }),
            email: classnames({
                    "Form__invalid": true,
                    "Form__invalid--show": this.state.fieldErrors.email
                    })
        };

        return(
            <form className="Form" onSubmit={this.onFormSubmit}>
                <div className="Form__wrapper">
                <label for="name">
                    Name* 
                </label>
                <input
                    id="name"
                    name="name"
                    type="text"
                    value={this.state.fields.name}
                    onChange={this.onInputChange}
                >
                </input>
                <div className={errorClass.name}>{this.state.fieldErrors.name}</div>
                <br />
                <label for="address">
                    Address*
                </label> 
                <input
                    id="address"
                    name="address"
                    type="text"
                    value={this.state.fields.address}
                    onChange={this.onInputChange}
                >
                </input>
                <div className={errorClass.address}>{this.state.fieldErrors.address}</div>
                <br />
                <label for="phone">
                    Phone 
                </label>
                <input
                    id="phone"
                    name="phone"
                    type="number"
                    value={this.state.fields.phone}
                    onChange={this.onInputChange}
                >
                </input>                    
                <br />
                <label for="email">
                    E-mail*
                </label>
                <input
                    id="email"
                    name="email"
                    type="text"
                    value={this.state.fields.email}
                    onChange={this.onInputChange}
                >
                </input>
                <div className={errorClass.email}>{this.state.fieldErrors.email}</div>
                <br />
                <label for="shippingOptions">
                    Shipping oprtons
                </label>
                <select 
                    id="shippingOptions"
                    name="shippingOptions"
                    value={this.state.fields.shippingOptions} 
                    onChange={this.onInputChange}>
                    <option value="FREE SHIPPING">Free shipping</option>
                    <option value="DHL">DHL</option>
                    <option value="POST">POST</option>
                </select>
                <br />
                <button type="submit">PAY</button>
                </div>
            </form>
        )
    }
}

export default Form;
