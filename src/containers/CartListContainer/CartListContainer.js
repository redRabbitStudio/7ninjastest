import { connect } from 'react-redux'
import { incrItem, decrItem, deleteItem } from '../../actions';

import CartList from '../../components/CartList/CartList.js';


const mapStateToProps = state => ({
  cartItems: state.cartItems
})

const mapDispatchToProps = dispatch => ({
  incrCount: id => dispatch(incrItem(id)),
  decrCount: id => dispatch(decrItem(id)),
  deleteItem: id => dispatch(deleteItem(id))

})

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(CartList)