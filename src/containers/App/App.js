import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import {BrowserRouter, Route} from 'react-router-dom';
import cartReducer from '../../reducers/cartReducer';
import CartListContainer from '../CartListContainer/CartListContainer';
import Form from '../../components/Form/Form';

import './App.css';

const store = createStore(
                    cartReducer,
                    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

class App extends Component {
  render() {
    return (
        <Provider store={store}>  
            <BrowserRouter>
              <div>
                <Route exact={true} path='/' render={() => (
                  <div className="App">
                    <CartListContainer/>
                  </div>
                )}/>
                <Route exact={true} path='/shipping' render={() => (
                  <div className="App">
                      <Form/>
                  </div>
                )}/>
              </div>
            </BrowserRouter>

        </Provider>
    );
  }
}

export default App;
