//action creators

export const incrItem = (id,count) =>({
    type: 'INCR_ITEM',
    id,
    count
})

export const decrItem = (id,count) =>({
    type: 'DECR_ITEM',
    id,
    count
})

export const deleteItem = (id) =>({
    type: 'DELETE_ITEM',
    id,
})